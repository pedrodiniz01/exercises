package com.company;

import java.util.Scanner;

public class exercicio7 {
    public static void main(String[] args) {
    // variaveis
        double distanciaManel, tempoManel, distanciaZe, tempoZe, velocidadeMedia;

        // Scanner
        Scanner ler = new Scanner(System.in);

        // Perguntas
        System.out.println("Qual a distancia que o Manel percorreu (em kms): ");
        distanciaManel = ler.nextDouble();
        System.out.println("Quanto tempo demorou o Manel (em horas): ");
        tempoManel = ler.nextDouble();
        System.out.println("Quanto demorou o Zé até desistir (em horas): ");
        tempoZe = ler.nextDouble();

        // Saída dados
        velocidadeMedia = distanciaManel/tempoManel;
        distanciaZe = velocidadeMedia * tempoZe;
        System.out.println(velocidadeMedia);
        System.out.println("O Zé percorreu: "+distanciaZe+" kms.");

    }
}

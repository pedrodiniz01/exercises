package com.company;

import java.util.Scanner;

public class exercicio9 {
    public static void main(String[] args) {
       // variaveis
        double c1,c2,h1,h2;

        // Scanner
        Scanner ler = new Scanner(System.in);

        // perguntas
        System.out.println("Qual o comprimento de c1: ");
        c1 = ler.nextDouble();
        System.out.println("Qual o comprimento de c2: ");
        c2 = ler.nextDouble();

        // processamento
        h1 = Math.pow(c1,2)+Math.pow(c2,2);
        h2 = Math.sqrt(h1);

        //
        System.out.println("O tamanho do outro lado é de: " + h2);
    }
}

package com.company;
import java.util.Scanner;

public class exercicio6 {
    public static void main(String[] args) {

        // variaveis
        double alturaEdificio, sombraEdificio, sombraPessoa, alturaPessoa;

        // scanner
        Scanner ler = new Scanner(System.in);

        // perguntas
        System.out.println("Qual a tua altura (em cms): ");
        alturaPessoa = ler.nextDouble();
        System.out.println("Qual o tamanho da tua sombra (em cms): ");
        sombraPessoa = ler.nextDouble();
        System.out.println("Qual o tamanho da sombra do edificio (em cms): ");
        sombraEdificio = ler.nextDouble();

        // saída dados
        alturaEdificio = ((alturaPessoa * sombraEdificio)/sombraPessoa)/100;
        System.out.println("A altura do edificio é de: "+alturaEdificio+" metros.");
    }
}

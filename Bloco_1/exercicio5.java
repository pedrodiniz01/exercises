package com.company;
import java.util.Scanner;

public class exericio5 {
    public static void main(String[] args) {

        // variaveis
        double tempo, distancia;

        // entrada de dados
        Scanner ler = new Scanner(System.in);

        // leitura de dados
        System.out.println("Quantos segundos até a pedra chegar ao chão: ");
        tempo = ler.nextDouble();

        // saída de dados
        distancia = (9.8*Math.pow(tempo,2))/2;
        System.out.println("A pedra está a: "+distancia+" metros.");
    }
}

package com.company;
import java.util.Scanner;

public class exercicio8 {
    public static void main(String[] args) {
    // variaveis
        double comprimento, largura, perimetro;

        // Scanner
        Scanner ler = new Scanner(System.in);

        // Perguntas
        System.out.println("Qual o comprimento: ");
        comprimento = ler.nextDouble();
        System.out.println("Qual a largura: ");
        largura = ler.nextDouble();

        // Processamento
        perimetro = 2*comprimento + 2*largura;

        //saida de dados
        System.out.println("O perimetro do retangulo é: " + perimetro);
    }
}

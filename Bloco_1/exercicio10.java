package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	//variaveis
        double raio, altura,volume, volumeLiquido;

        // leitura dados
        Scanner ler= new Scanner(System.in);

        // introduzir dados
        System.out.println("Qual é a raio: ");
        raio = ler.nextDouble();

        System.out.println("Qual é a altura: ");
        altura = ler.nextDouble();

        // dados saída
        volume = raio*raio*altura*Math.PI;
        volumeLiquido= volume*1000;
        System.out.println("O volume do cilindro é: "+volume+" m3.");
        System.out.println("O volume do cilindro é: "+volumeLiquido+" l.");

    }
}

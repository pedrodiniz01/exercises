package com.company;

import java.util.Scanner;

public class exercicio4 {
    public static void main(String[] args) {

        // variaveis
        double tempo, distancia, velocidadeLuz = 1224000/3600;

        // entrada dados
        Scanner ler = new Scanner(System.in);

        // leitura de dados
        System.out.println("Quanto tempo demoraste até ouvir o relampago? (em segundos)");
        tempo = ler.nextDouble();

        //saída de dados
        distancia = tempo * velocidadeLuz;
        System.out.println("Estas a " + distancia + " metros.");

    }
}

public class ex15 {
    public static void main(String[] args) {
        System.out.println(5 % 3);
    }

    /**
     * Isto faz isto...
     * @param array - le um vetor de 1 dimensao
     * @return Retorna um int com o menor valor do vetor.
     * @author Ghetto_Tales
     */

    public static int lowerValue(int[][] array) {

        int lowerValue = array[0][0];

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (array[i][k] < lowerValue) {
                    lowerValue = array[i][k];
                }
            }

        }
        return lowerValue;
    }

    public static int higherValue(int[][] array) {

        int higherValue = array[0][0];

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (array[i][k] > higherValue) {
                    higherValue = array[i][k];
                }
            }
        }
        return higherValue;
    }

    public static double averageValue(int[][] array) {

        double somador = 0;
        double c = 0;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                somador = somador + array[i][k];
                c++;
            }
        }
        return somador / c;
    }

    public static int ProductOfArray(int[][] array) {

        int prod = 1;


        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                prod = prod * array[i][k];
            }
        }
        return prod;
    }

    public static int[] noDuplicatesFromArray(int[][] array) {

        int c = 0;

        // ver repetidos

        int a = 0;
        int b = 0;
        c = 0;
        int cfinal = 0;

        while (a < array.length) {
            while (b < array[a].length) {
                for (int i = 0; i < array.length; i++) {
                    for (int k = 0; k < array[i].length; k++) {
                        if (array[a][b] == array[i][k]) {
                            c++;
                        }
                    }

                }
                if (c == 1) {
                    cfinal++;
                    c = 0;
                }
                b++;
            }
            a++;
        }

        int[] newArray = new int[cfinal];

        a = 0;
        b = 0;
        c = 0;
        int p = 0;

        while (a < array.length) {
            while (b < array[a].length) {
                for (int i = 0; i < array.length; i++) {
                    for (int k = 0; k < array[i].length; k++) {
                        if (array[a][b] == array[i][k]) {
                            c++;
                        }
                    }

                }
                if (c == 1) {
                    newArray[p] = array[a][b];
                    c = 0;
                    p++;
                }
                b++;
            }
            a++;
        }

        return newArray;

    }

    public static int[] primeNumbers2dArray(int[][] array) {

        int higherValue = array[0][0];

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (array[i][k] > higherValue) {
                    higherValue = array[i][k];
                }
            }
        }

        int l = 1;
        int c = 0;
        int cfinal = 0;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                while (l <= higherValue) {
                    if (array[i][k] % l == 0) {
                        c++;
                    }
                    l++;
                }
                l = 1;
                if (c == 2) {
                    cfinal++;
                }
                c = 0;
            }
            l = 1;
            if (c == 2) {
                cfinal++;
            }
            c = 0;


        }

        int[] newArray = new int[cfinal];

        l = 1;
        c = 0;
        int u = 0;
        cfinal = 0;

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                while (l <= higherValue) {
                    if (array[i][k] % l == 0) {
                        c++;
                    }
                    l++;
                }
                l = 1;
                if (c == 2) {
                    newArray[u] = array[i][k];
                    u++;
                }
                c = 0;
            }
            l = 1;
            if (c == 2) {
                cfinal++;
            }
            c = 0;


        }

        return newArray;
    }

    public static int[] MainDiagonal(int[][] array) {

        int[] newArray = new int[array.length];

        if (ex12.NumberOfColumns(array) == array[0].length) {
            for (int i = 0; i < array.length; i++) {
                newArray[i] = array[i][i];
            }

        }
        return newArray;
    }

    public static int[] SecondaryDiagonal(int[][] array) {

        int[] newArray = new int[array.length];
        int u = 0;

        if (ex12.NumberOfColumns(array) == array[0].length) {
            for (int i = array.length - 1; i >= 0; i--) {
                newArray[u] = array[i][u];
                u++;
            }
        }
        return newArray;

    }

    public static boolean matrizIdentidade(int[][] array) {

        for (int i = 0; i < array.length; i++) {
            if (array[i][i] == 1) {
            } else {
                return false;
            }

        }

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                if (i != k) {
                    if (array[i][k] == 0) {
                    } else {
                        return false;
                    }
                }
            }

        }
        return true;
    }

    public static double[][] invertArray2d(double[][] array) {

        int i = 0;
        int k = 0;
        double[][] inverseArray = new double[array.length][array.length];
        int iLen = array.length;
        int kLen = array[i].length;
        double adj = (1) / ((array[0][0] * array[1][1]) - (array[0][1]) * array[1][0]);


        if ((array.length == 2)) {
            for (i = 0; i < array.length; i++) {
                for (k = 0; k < array[i].length; k++) {
                    if (i == k) {
                        inverseArray[iLen - 1][kLen - 1] = array[i][k] * adj;
                        iLen = iLen - 1;
                        kLen = kLen - 1;
                    } else {
                        inverseArray[i][k] = -array[i][k] * adj;
                    }
                }
            }
        }
        return inverseArray;
    }

    public static int[][] array2dTranspose(int[][] array) {

        int[][] newArray = new int[0][0];

        int l = 0;
        int v = 0;

        if ((ex13.IsArraySquare(array) == true) || (ex14.isArrayRetangule(array) == true)) {
            newArray = new int[array[0].length][array.length];
            for (int i = 0; i < array.length; i++) {
                for (int k = 0; k < array[i].length; k++) {
                    newArray[v][l] = array[i][k];
                    v++;
                }
                v=0;
                l++;
            }

        } return newArray;
    }
}

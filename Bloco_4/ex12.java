public class ex12 {

    public static int NumberOfColumns(int[][] array) {
        int len = array[0].length;
        int lenFinal = len;

        for (int i = 0; i < array.length; i++) {
            if (array[i].length == len) {
                len = array[i].length;

            } else {
                len = -1;

                break;
            }
        } return len;
    }
}

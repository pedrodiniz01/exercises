public class ex4 {
    public static int[] EvenArray(int[] array) {
        int k = 0;
        int c = 0;
        for (int i = 0; i <= array.length - 1; i++) {
            if (array[i] % 2 == 0) {
                c++;
            }
        }
        int[] evenArray = new int[c];
        for (int l = 0; l <= array.length - 1; l++) {
            if (array[l] % 2 == 0) {
                evenArray[k] = array[l];
                k++;
            }
        } return evenArray;
    }
    public static int[] OddArray(int[] array) {
        int k = 0;
        int c = 0;
        for (int i = 0; i <= array.length - 1; i++) {
            if (array[i] % 2 != 0) {
                c++;
            }
        }
        int[] oddArray = new int[c];
        for (int l = 0; l <= array.length - 1; l++) {
            if (array[l] % 2 != 0) {
                oddArray[k] = array[l];
                k++;
            }
        } return oddArray;
    }
}


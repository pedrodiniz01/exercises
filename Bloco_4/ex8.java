public class ex8 {
    public static int[] MultiplesInInterval(int lowerRange, int higherRange, int multipleOne, int multipleTwo) {
        int c = 0;
        for (int i = lowerRange; i <= higherRange; i++) {
            if ((i % multipleOne == 0) && (i % multipleTwo == 0)) {
                c++;
            }
        }
        int k = 0;

        int[] MultipliesInIntervale = new int[c];
        for (int i = lowerRange; i <= higherRange; i++) {
            if ((i % multipleOne == 0) && (i % multipleTwo == 0)) {
                MultipliesInIntervale[k] = i;
            }
        } return MultipliesInIntervale;
    }
}

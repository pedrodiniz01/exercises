public class ex10 {
    public static void main(String[] args) {

    }

    public static int LowerValue(int[] array) {
        int lowerValue = 999999999;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < lowerValue) {
                lowerValue = array[i];
            }

        }
        return lowerValue;
    }

    public static int HigherValue(int[] array) {
        int higherValue = -999999999;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > higherValue) {
                higherValue = array[i];
            }
        }
        return higherValue;
    }

    public static double AverageOfArray(int[] array) {
        double somador = 0;
        double c = 0;
        for (int i = 0; i < array.length; i++) {
            somador = array[i] + somador;
            c++;
        }
        return somador / c;
    }

    public static int ProductOfArray(int[] array) {
        int prod = 1;
        for (int i = 0; i < array.length; i++) {
            prod = array[i] * prod;
        }
        return prod;
    }

    public static int[] NoDuplicatesFromArray(int[] array) {
        int l = 0;
        int i = 0;
        int k = 0;
        int contador = 0;
        int contadorFinal = 0;

        while (k < array.length) {
            for (i = 0; i < array.length; i++) {
                if (array[l] == array[i]) {
                    contador++;
                }

            }
            if (contador == 1) {
                contadorFinal++;
            }
            l++;
            k++;
            contador = 0;
        }
        int[] finalArray = new int[contadorFinal];
        k = 0;
        l = 0;

        while (k < finalArray.length) {
            for (i = 0; i < array.length; i++) {
                if (array[l] == array[i]) {

                    contador++;
                }

            }
            if (contador == 1) {
                finalArray[k] = array[l];
                k++;
            }
            l++;

            contador = 0;
        }
        return finalArray;

    }

    public static int[] InvertArray(int[] array) {
        int[] invertArray = new int[array.length];
        int k = 0;

        for (int i = array.length - 1; i >= 0; i--) {
            invertArray[i] = array[k];
            k++;
        }
        return invertArray;
    }

    public static int[] PrimeNumbers(int[] array) {
        int k = 0;
        int contador = 0;
        int contadorFinal = 0;
        while (k < array.length) {
            for (int i = 1; i <= 9; i++) {
                if (array[k] % i == 0) {
                    contador++;
                }
            }
            if (contador == 2) {
                contadorFinal++;
            }
            contador = 0;
            k++;
        }

        int[] newArray = new int[contadorFinal];
        k = 0;
        int l=0;
        contadorFinal=0;
        contador=0;

        while (k < array.length) {
            for (int i = 1; i <= 9; i++) {
                if (array[k] % i == 0) {
                    contador++;
                }
            }
            if (contador == 2) {
                newArray[l] = array[k];
                l++;
            }
            contador = 0;
            k++;
        }
        return newArray;
    }
}

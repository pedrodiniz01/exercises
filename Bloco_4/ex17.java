public class ex17 {
    public static void main(String[] args) {

        int[][] array1 = {{1,2},{3,4}};
        int[][] array2 = {{1,2},{3,4}};

        System.out.println(newMultiply2dArray(array1,array2));
    }
    public static int[][] array2dMultiplied(int array[][], int num) {

        int newArray[][] = new int[array.length][array[0].length];

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                newArray[i][k] = array[i][k] * num;
            }
        }
        return newArray;
    }

    public static int[][] sum2dArray(int array[][], int arrayTwo[][]) {

        int[][] newArray = new int[array.length][array[0].length];

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                newArray[i][k] = array[i][k] + arrayTwo[i][k];
            }
        }
        return newArray;
    }

    public static int[][] multiply2dArray(int array[][], int arrayTwo[][]) {

        int[][] newArray = new int[array.length][array[0].length];

        for (int i = 0; i < array.length; i++) {
            for (int k = 0; k < array[i].length; k++) {
                newArray[i][k] = array[i][k] * arrayTwo[i][k];
            }
        }
        return newArray;
    }

    public static int newMultiply2dArray(int[][] array1, int[][] array2) {

        int result = 0;
        int u = 0;

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                result = result + array1[i][j] * array2[j][u];
            }
            u++;
        }
        return result;
    }
}

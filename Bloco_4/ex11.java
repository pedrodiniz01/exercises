public class ex11 {

    public static int ProdutoEscalar(int[] arrayUm, int[] arrayDois) {
        int prod = 0;
        for (int i = 0; i < arrayUm.length; i++) {
            prod = prod + (arrayUm[i]*arrayDois[i]);
        }
        return prod;
    }
}

import java.util.Arrays;
import java.util.Random;

public class ex18 {
    public static void main(String[] args) {


        String[][] nova =
                       {{"a", "b", "c"},
                        {"d", "e", "f"},
                        {"g", "h", "i"}};

        /* System.out.println(Arrays.deepToString(matriz));*/
        System.out.println(firstLetter(nova, "ea"));


    }

    public static String firstLetter(String[][] matrix, String wantedWord) {

        String wordArray[] = wantedWord.split("");
        String compareWord[] = new String[wordArray.length];
        String result = "";
        int l = 0;
        int v = 0;
        int n = 0;
        int newI = 0;
        int newJ = 0;
        int u = 0;
        int posicaoInicialX = 0;
        int posicaoInicialY = 0;
        int posicaoFinalX = 0;
        int posicaoFinalY = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j].equals(wordArray[0])) {
                    if ((i >= wordArray.length - 1)) // cima
                    {
                        u = 0;
                        newJ = j;
                        newI = i;
                        l = 0;
                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI + l][newJ];
                            l--;
                        }

                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = l-l;
                            posicaoFinalY = newJ;
                        }
                    }
                    if (i + wordArray.length - 1 <= matrix.length) // baixo
                    {
                        newJ = j;
                        newI = i;
                        l = 0;
                        u = 0;

                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI + l][newJ];
                            l++;
                        }
                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI + l-1;
                            posicaoFinalY = newJ;
                        }
                    }
                    if (j + wordArray.length - 1 <= matrix.length) // direita
                    {
                        newJ = j;
                        newI = i;
                        l = 0;
                        u = 0;

                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI][newJ + l];
                            l++;
                        }
                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI;
                            posicaoFinalY = newJ + l-1;
                        }
                    }
                    if (j >= wordArray.length - 1) // esquerda
                    {
                        newI = i;
                        newJ = j;
                        l = 0;
                        u = 0;

                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI][newJ + l];
                            l--;
                        }
                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI;
                            posicaoFinalY = newJ + l+1;
                        }
                    }
                    if ((i >= wordArray.length - 1) && (j + wordArray.length - 1 <= matrix.length)) // cima direita
                    {
                        newI = i;
                        newJ = j;
                        u = 0;
                        l = 0;
                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI + v][newJ + l];
                            l++;
                            v--;
                        }

                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI + v;
                            posicaoFinalY = newJ + l;
                        }
                    }
                    if ((i >= wordArray.length - 1) && (j >= wordArray.length - 1)) // cima esquerda
                    {
                        newI = i;
                        newJ = j;
                        l = 0;
                        v = 0;
                        u = 0;

                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI + v][newJ + l];
                            l--;
                            v--;
                        }

                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI + v;
                            posicaoFinalY = newJ + l;
                        }
                    }
                    if ((i + wordArray.length - 1 <= matrix.length) && (j + wordArray.length - 1 <= matrix.length)) // baixo direita
                    {
                        newI = i;
                        newJ = j;
                        l = 0;
                        u = 0;
                        v = 0;

                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI + v][newJ + l];
                            l++;
                            v++;
                        }

                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI + v;
                            posicaoFinalY = newJ + l;
                        }
                    }
                    if ((i + wordArray.length - 1 <= matrix.length) && (j >= wordArray.length - 1)) // baixo esquerda
                    {
                        newI = i;
                        newJ = j;
                        l = 0;
                        u = 0;
                        v = 0;

                        for (u = 0; u < wordArray.length; u++) {
                            compareWord[u] = matrix[newI + v][newJ + l];
                            l--;
                            v++;
                        }
                        if (Arrays.equals(compareWord, wordArray)) {
                            result = "Palavra encontrada";
                            posicaoInicialX = i;
                            posicaoInicialY = j;
                            posicaoFinalX = newI + v;
                            posicaoFinalY = newJ + l;
                        }
                    }
                }
            }

        }
        if (result == "Palavra encontrada") {
            result = result + "!\nInicio da palavra:\nlinha = " + String.valueOf(posicaoInicialX) + " e coluna = " + String.valueOf(posicaoInicialY) + "\nFim da palavra:\nlinha = " + String.valueOf(posicaoFinalX) + " e coluna = " + String.valueOf(posicaoFinalY);
        } else {
            result = "Palavra não encontrada!";
        }
        return result;
    }
}

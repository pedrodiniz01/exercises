import java.util.Scanner;

public class ex4 {
    public static void main(String[] args) {

    }

    public static int[] listaNumeros() {
        int[] listaNumeros = {2, 3, -5, 4, 15, 3, 5,8};
        return listaNumeros;
    }

    public static int numeroDeMultiplosTresAlineaA(int[] listaNumeros) {
        int contadorMultiplosDeTres = 0;
        for (int i = 0; i < listaNumeros.length; i++) {
            if ((listaNumeros[i]) % 3 == 0) {
                contadorMultiplosDeTres++;
            }
        }
        return contadorMultiplosDeTres;
    }

    public static int numeroDeMultiplosDadoNumeroAlineaB(int numero,int[] listaNumeros) {
        int contadorMultiplosDeDadoNumero = 0;
        for (int i = 0; i < listaNumeros.length; i++) {
            if ((listaNumeros[i]) % numero == 0) {
                contadorMultiplosDeDadoNumero++;
            }
        }
        return contadorMultiplosDeDadoNumero;
    }
    public static int numeroDeMultiplosTresECincoAlineaC(int[] listaNumeros) {
        int contadorMultiplosDeTresECinco = 0;
        for (int i = 0; i < listaNumeros.length; i++) {
            if (((listaNumeros[i]) % 3 == 0) && ((listaNumeros[i]) % 5 == 0)) {
                contadorMultiplosDeTresECinco++;
            }
        }
        return contadorMultiplosDeTresECinco++;
    }
    public static int numeroDeMultiplosDeDoisNumerosAlineaD(int numero1, int numero2, int[] listaNumeros) {
        int contadorMultiplosDeDadoNumero = 0;
        for (int i = 0; i < listaNumeros.length; i++) {
            if (((listaNumeros[i]) % numero1 == 0) && ((listaNumeros[i]) % numero2 == 0)) {
                contadorMultiplosDeDadoNumero++;
            }
        }
        return contadorMultiplosDeDadoNumero++;
    }

    public static int somaDoMultiploDoisNumeroAlineaE(int numero1, int numero2, int[] listaNumeros) {
        int SomadorDeDadoNumero = 0;
        for (int i = 0; i < listaNumeros.length; i++) {
            if (((listaNumeros[i]) % numero1 == 0) && ((listaNumeros[i]) % numero2 == 0)) {
                SomadorDeDadoNumero = SomadorDeDadoNumero + (listaNumeros[i]);
            }
        }
        return SomadorDeDadoNumero;
    }
}

public class ex19 {
    public static void main(String[] args) {
        System.out.println(ReorganizeEvenAndOdds(122123223));
    }

    public static String ReorganizeEvenAndOdds(int number) {
        int reversed = 0;
        int evenNumber = 0;
        int oddNumber = 0;
        int countOddNumber = 0;
        int finalNumber = 0;
        while (number > 0) {
            reversed = number % 10;
            if (reversed % 2 == 0) {
                evenNumber = evenNumber * 10 + reversed;

            } else {
                oddNumber = oddNumber * 10 + reversed;
                countOddNumber++;
            }
            number = number / 10;

        }
        finalNumber = (int) (evenNumber * Math.pow(10, countOddNumber)) + oddNumber;
        return "Par: " + evenNumber + " Impar: " + oddNumber + " Número final: " + finalNumber + " ";
    }
}


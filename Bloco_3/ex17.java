public class ex17 {
    public static void main(String[] args) {

    }

    public static String IsFoodEnough(int weightKg, int amountFood) {
        while (weightKg < 0) {
            if (weightKg <= 10 && amountFood >= 100) {
                return "Raça pequena: Quantidade correta.";
            } else if ((weightKg > 10 && weightKg <= 25) && amountFood >= 250) {
                return "Raça média: Quantidade correta.";
            } else if ((weightKg > 25 && weightKg <= 45) && amountFood >= 300) {
                return "Raça grande: Quantidade correta.";
            } else if ((weightKg >= 45) && amountFood >= 500) {
                return "Raça gigante: Quantidade correta.";
            }

        } return "Quantidade de ração incorreta para a raça.";
    }
}

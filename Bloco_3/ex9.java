import java.util.Scanner;

public class ex9 {
    public static void main(String[] args) {
        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        int salary=0;
        int extraHours=0;
        int countSalary=0;
        int sumSalary=0;

        while (extraHours != -1) {
            // pedir dados
            System.out.println("Qual o salário base: ");
            salary = ler.nextInt();
            countSalary++;

            System.out.println("Quantas horas extras fez: ");
            extraHours = ler.nextInt();


            // saída dados
            System.out.println("O salário será de: " + finalSalary(salary,extraHours));
            sumSalary= (int) (finalSalary(salary,extraHours)+ sumSalary);
        }
        System.out.println("\nPrograma terminado.");
        System.out.println("\nA média dos salários é de: " + averageSalary(sumSalary,countSalary));

    }
    public static double finalSalary(int salary, int extraHours) {

        double finalSalary = salary + (extraHours * 0.02) * salary;
        return finalSalary;
    }
    public static double averageSalary(int sumSalary,int countSalary) {
        return sumSalary/countSalary;
    }
}

import java.util.Scanner;

public class ex15 {
    public static void main(String[] args) {
        // scanner
        Scanner ler = new Scanner(System.in);

        //variaveis
        int nota = 0;

        // pedir dados
        while (nota >= 0) {
            System.out.println("Introduza uma nota: ");
            nota = ler.nextInt();
            if(nota<0) {
                break;
            }
            System.out.println(Qualification(nota));

        }
        System.out.println("Programa terminado.");
    }

    public static String Qualification(int nota) {
        if (nota >= 0 && nota <= 4) {
            return "Mau";
        } else if (nota >= 5 && nota <= 9) {
            return "Medíocre";
        } else if (nota >= 10 && nota <= 13) {
            return "Suficiente";
        } else if (nota >= 14 && nota <= 17) {
            return "Bom";
        } else if (nota >= 18 && nota <= 20) {
            return "Muito Bom";
        }
        return "Nota incorreta";
    }

}

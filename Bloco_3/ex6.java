public class ex6 {
    public static void main(String[] args) {

    }

    public static int alineaA(int numero) {
        String numeroString = Integer.toString(numero);
        int contador = 0;
        for (int i = 0; i < numeroString.length(); i++) {
            contador++;
        }
        return contador;
    }

    public static int alineaB(int number) {
        int dividend = number;
        int remainder = 0;
        int contador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;
            if (remainder % 2 == 0) {
                contador++;
            }
            dividend = dividend / 10;
        }
        return contador;
    }

    public static int alineaC(int number) {
        int dividend = number;
        int remainder = 0;
        int contador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;
            if (remainder % 2 != 0) {
                contador++;
            }
            dividend = dividend / 10;
        }
        return contador;
    }

    public static int alineaD(int number) {
        int dividend = number;
        int remainder = 0;
        int somador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;

            somador = somador + remainder;

            dividend = dividend / 10;
        }
        return somador;
    }

    public static int alineaE(int number) {
        int dividend = number;
        int remainder = 0;
        int somador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;
            if (remainder % 2 == 0) {
                somador = somador + remainder;
            }
            dividend = dividend / 10;
        }
        return somador;
    }

    public static int alineaF(int number) {
        int dividend = number;
        int remainder = 0;
        int somador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;
            if (remainder % 2 != 0) {
                somador = somador + remainder;
            }
            dividend = dividend / 10;
        }
        return somador;
    }

    public static int alineaG(int number) {
        int dividend = number;
        int remainder = 0;
        int somador = 0;
        int contador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;

            somador = somador + remainder;
            contador++;

            dividend = dividend / 10;
        }
        return somador / contador;
    }

    public static int alineaH(int number) {
        int dividend = number;
        int remainder = 0;
        int somador = 0;
        int contador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;
            if (remainder % 2 == 0) {
                somador = somador + remainder;
                contador++;
            }
            dividend = dividend / 10;
        }
        return somador / contador;
    }

    public static int alineaI(int number) {
        int dividend = number;
        int remainder = 0;
        int somador = 0;
        int contador = 0;
        while (dividend > 0) {
            remainder = (int) dividend % 10;
            if (remainder % 2 != 0) {
                somador = somador + remainder;
                contador++;
            }
            dividend = dividend / 10;
        }
        return somador / contador;
    }
// 789
    public static long alineaJ(long number) {
        long reversed = 0;
        while (number != 0) {
            reversed = reversed * 10 + (number % 10);
            number = number / 10;

        } return reversed;

    }

}


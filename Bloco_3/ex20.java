public class ex20 {
    public static void main(String[] args) {
        System.out.println(NumberClassification(0));
    }

    public static String NumberClassification(int number) {
        int sumNumber = 0;
        for (int i = 1; i <= 9; i++) {
            if (number % i == 0 && number!=i) {
                sumNumber = sumNumber + i;
            }
        } if( sumNumber == number) {
            return "Perfeito";
        } else if (sumNumber > number) {
            return "Abundantes";
        } else if (sumNumber < number) {
            return "Reduzidos";
        }
        return null;
    }
}

import java.util.Scanner;

public class ex10 {
    public static void main(String[] args) {
        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        int highestRange = 0;
        int number = 0;
        int multiplyNumber = 1;
        int highestNumber = -11111111;


        System.out.println("Introduza o intervalo superior: ");
        highestRange = ler.nextInt();

        while (multiplyNumber < highestRange) {
            System.out.println("Introduza um número: ");
            number = ler.nextInt();
            multiplyNumber = multiplyNumber * number;
            if (number > highestNumber) {
                highestNumber = number;
            }
        }
        System.out.println(highestNumber);
    }
}

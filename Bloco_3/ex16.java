import java.util.Scanner;

public class ex16 {
    public static void main(String[] args) {
        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        int salary=0;

        while(salary>=0) {
            // pedir dados
            System.out.println("Introduza o seu salário: ");
            salary = ler.nextInt();
            if(salary<0) {
                break;
            }
            System.out.println("O seu salário líquido é: " + LiquidSalary(salary));
        }
        System.out.println("Programa terminado.");
    }

    public static double LiquidSalary(int salary) {
        if (salary <= 500) {
            return salary * 0.9;
        } else if (salary > 500 && salary <= 1000) {
            return (500 * 0.90)+(salary-500)*0.85;
        } else if (salary > 1000) {
            return (500 * 0.90)+((500)*0.85)+ (salary-1000)*0.8;
        }
        return 0;
    }
}

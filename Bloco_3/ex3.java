import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        System.out.println(percentagemPares());

    }

    public static double percentagemPares() {
        Scanner ler = new Scanner(System.in);
        double numero = 0;
        double contadorPar = 0;
        double contadorTotal = 0;
        do {
            System.out.println("Introduza um valor: ");
            numero = ler.nextInt();
            if (numero % 2 == 0) {
                contadorPar = contadorPar + 1;

            }
            contadorTotal = contadorTotal + 1;
        } while (numero >= 0);
        return contadorPar / contadorTotal;
    }

    public static double mediaImpares() {
        Scanner ler = new Scanner(System.in);
        double numero = 0;
        double somaImpar = 0;
        double contadorImpar = 0;
        do {
            System.out.println("Introduza um valor: ");
            numero = ler.nextInt();
            if (numero % 2 != 0) {
                somaImpar = numero + somaImpar;
            }
            contadorImpar = contadorImpar + 1;
        } while (numero >= 0);
        return somaImpar/contadorImpar;

    }

}

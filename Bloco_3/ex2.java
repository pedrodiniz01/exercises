import java.util.Scanner;

public class ex2 {
    public static void main(String[] args) {


        System.out.println(percentagemPositivas(encherArrayComNotas(numAlunos())));

        System.out.println(mediaDasNegativas(encherArrayComNotas(numAlunos())));

    }

    public static boolean notasPositivas(int notas) {
        if ((notas >= 0) && (notas <= 20)) {
            return true;
        }
        return false;
    }

    public static boolean numAlunosPositivos(int numAlunos) {
        if (numAlunos >= 0) {
            return true;
        }
        return false;
    }

    public static int numAlunos() {
        Scanner ler = new Scanner(System.in);
        int numAlunos =0;
        System.out.println("Introduza o número de alunos: ");
        numAlunos = ler.nextInt();
        while (numAlunosPositivos(numAlunos) == false) {
            System.out.println("Erro! Não pode introduzir números negativos!\nIntroduza novamente o número de alunos: ");
            numAlunos = ler.nextInt();
        }
        return numAlunos;
    }

    public static int[] encherArrayComNotas(int numAlunos) {
        Scanner ler = new Scanner(System.in);
        int notaAluno;
        int[] notasAlunos = new int[numAlunos];

        for (int i = 0; i < notasAlunos.length; i++) {
            System.out.println("Introduza a nota do aluno "+ (i+1)+":");
            notaAluno = ler.nextInt();
            notasAlunos[i] = notaAluno;
        }
        return notasAlunos;
    }
    public static double percentagemPositivas(int[] notasAlunos) {
        double numPositivas=0;
        double total = notasAlunos.length;
        for(int i=0; i < notasAlunos.length; i++) {
            if(notasAlunos[i]>=10) {
                numPositivas = numPositivas +1;
            }
        }
        return numPositivas/total;
    }
    public static double mediaDasNegativas(int[] notasAlunos) {
        double total = notasAlunos.length;
        double totalNotasNegativas =0;
        int contador=0;
        for(int i=0; i < notasAlunos.length; i++) {
            if(notasAlunos[i]<10) {
                totalNotasNegativas = totalNotasNegativas+ notasAlunos[i];
                contador = contador + 1;
            }
        } return totalNotasNegativas/contador;

    }

}

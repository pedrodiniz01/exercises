import java.util.Scanner;

public class ex14 {
    public static void main(String[] args) {
        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        double quantity = 0;
        double quantityConversion = 0;

        while (quantity >= 0) {
            // pedir dados
            System.out.println("Quanto dinheiro quer converter: ");
            quantity = ler.nextInt();
            if (quantity < 0) {
                break;
            }

            System.out.println("\nPara que moeda quer converter:\n\nDolar [D]\nLibra [L]\nIene  [I]\nCoroa Sueca  [SC]\nFranco Suiço [FS]\n\nEscolha uma opção:");
            String option = ler.next();

            System.out.println("Valor da conversão: " + QuantityConversion(quantity, option));
        }
        System.out.println("\nPrograma terminado. ");
    }

    public static double QuantityConversion(double quantity, String option) {
        double result = 0;
        if (option.equalsIgnoreCase("D")) {
            result = quantity * 1.534;
        } else if (option.equalsIgnoreCase("L")) {
            result = quantity * 0.774;
        } else if (option.equalsIgnoreCase("I")) {
            result = quantity * 161.480;
        } else if (option.equalsIgnoreCase("CS")) {
            result = quantity * 9.593;
        } else if (option.equalsIgnoreCase("FS")) {
            result = quantity * 1.601;
        }
        return result;
    }
}

public class ex5 {
    public static void main(String[] args) {

    }

    public static int somaParesAlineaA(int numero1, int numero2) {
        int somadorPares = 0;
        for (int i = numero1; i <= numero2; i++) {
            if (i % 2 == 0) {
                somadorPares = somadorPares + i;
            }
        }
        return somadorPares;
    }

    public static int contarParesAlineaB(int numero1, int numero2) {
        int contarPares = 0;
        for (int i = numero1; i <= numero2; i++) {
            if (i % 2 == 0) {
                contarPares++;
            }
        }
        return contarPares;
    }

    public static int somaImparesAlineaC(int numero1, int numero2) {
        int somadorPares = 0;
        for (int i = numero1; i <= numero2; i++) {
            if (i % 2 != 0) {
                somadorPares = somadorPares + i;
            }
        }
        return somadorPares;
    }

    public static int contarimparesAlineaD(int numero1, int numero2) {
        int contarImpares = 0;
        for (int i = numero1; i <= numero2; i++) {
            if (i % 2 != 0) {
                contarImpares++;
            }
        }
        return contarImpares;
    }

    public static int SomaNumerosMultiplosDeDadoNumero(int numero1, int intervalorInferior, int intervalorSuperior) {
        int somarNumeros = 0;
        if (intervalorInferior < intervalorSuperior) {
            for (int i = intervalorInferior; i <= intervalorSuperior; i++) {
                if (i % numero1 == 0) {
                    somarNumeros = somarNumeros + i;
                }
            }

        } else if (intervalorInferior > intervalorSuperior) {
            for (int i = intervalorSuperior; i <= intervalorInferior; i++) {
                if (i % numero1 == 0) {
                    somarNumeros = somarNumeros + i;
                }
            }
        }
        return somarNumeros;
    }
    public static int AlineaF(int numero1, int intervalorInferior, int intervalorSuperior) {
        int produtoNumeros = 1;
        if (intervalorInferior < intervalorSuperior) {
            for (int i = intervalorInferior; i <= intervalorSuperior; i++) {
                if (i % numero1 == 0) {
                    produtoNumeros = produtoNumeros * i;
                }
            }

        } else if (intervalorInferior > intervalorSuperior) {
            for (int i = intervalorSuperior; i <= intervalorInferior; i++) {
                if (i % numero1 == 0) {
                    produtoNumeros = produtoNumeros * i;
                }
            }
        }
        return produtoNumeros;
    }
    public static int AlineaG(int numero1, int intervalorInferior, int intervalorSuperior) {
        int somaNumeros = 0;
        int contador = 0;
        if (intervalorInferior < intervalorSuperior) {
            for (int i = intervalorInferior; i <= intervalorSuperior; i++) {
                if (i % numero1 == 0) {
                    somaNumeros = somaNumeros + i;
                    contador++;
                }
            }

        } else if (intervalorInferior > intervalorSuperior) {
            for (int i = intervalorSuperior; i <= intervalorInferior; i++) {
                if (i % numero1 == 0) {
                    somaNumeros = somaNumeros + i;
                    contador++;
                }
            }
        }
        return somaNumeros/contador;
    }

    public static int AlineaH(int numero1,int numero2, int intervalorInferior, int intervalorSuperior) {
        int somaNumeros = 0;
        int contador = 0;
        if (intervalorInferior < intervalorSuperior) {
            for (int i = intervalorInferior; i <= intervalorSuperior; i++) {
                if ((i % numero1 == 0) ||(i % numero2 == 0)) {
                    somaNumeros = somaNumeros + i;
                    contador++;
                }
            }

        } else if (intervalorInferior > intervalorSuperior) {
            for (int i = intervalorSuperior; i <= intervalorInferior; i++) {
                if ((i % numero1 == 0) ||(i % numero2 == 0)) {
                    somaNumeros = somaNumeros + i;
                    contador++;
                }
            }
        }
        return somaNumeros/contador;
    }

}

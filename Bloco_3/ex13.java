import java.util.Scanner;

public class ex13 {
    public static void main(String[] args) {
        System.out.println(ProductClassification());
    }

    public static String ProductClassification() {
        Scanner ler = new Scanner(System.in);
        int option = -2;

        while (option != 0) {
            System.out.println("Digite um código de 1 a 15: ");
            option = ler.nextInt();
            if (option == 1) {
                System.out.println("Alimento não perecível.");
            } else if (option >= 2 && option <= 4) {
                System.out.println("Alimento perecível.");
            } else if (option >= 5 && option <= 6) {
                System.out.println("Vestuário.");
            } else if (option == 7) {
                System.out.println("Higiene pessoal.");
            } else if (option >= 8 && option <= 15) {
                System.out.println("Limpeza e utensílios domésticos.");
            } else if (option < 0 && option > 15) {
                System.out.println("Código invalido.");
            }
        }
        return "Programa terminado.";
    }


}

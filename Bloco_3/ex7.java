public class ex7 {
    public static void main(String[] args) {

    }

    // alinea a)
    public static boolean IsPalindrom(long number) {
        long reversed = 0;
        long originalNumber = number;
        while (number > 0) {
            reversed = reversed * 10 + (number % 10);
            number = number / 10;
        }
        return (reversed == originalNumber);
    }

    // alinea b)
    public static boolean IsArmstrongNumber(long number) {
        long sumOfCubes = 0;
        long finalNumber = number;
        while (number > 0) {
            sumOfCubes = (long) (sumOfCubes + Math.pow(number % 10, 3));
            number = number / 10;
        }
        return (sumOfCubes == finalNumber);
    }

    // alinea c)
    public static long FirstPalindromInInterval(int lowestRange, int highestRange) {
        int i = lowestRange;
        long originalNumber = 0;
        for (i = lowestRange; i <= highestRange; i++) {
            long reversed = 0;
            originalNumber = i;
            while (originalNumber > 0) {
                reversed = reversed * 10 + (originalNumber % 10);
                originalNumber = originalNumber / 10;
            }
            if ((reversed == i)) {
                break;
            }
            reversed = 0;
            originalNumber = i;

        }

        return i;
    }

    // alinea d)

    public static long HighestPalindromInInterval(int lowestRange, int highestRange) {
        int i = lowestRange;
        long originalNumber = 0;
        long highestPalindrom = 0;
        for (i = lowestRange; i <= highestRange; i++) {
            long reversed = 0;
            originalNumber = i;
            while (originalNumber > 0) {
                reversed = reversed * 10 + (originalNumber % 10);
                originalNumber = originalNumber / 10;
            }
            if ((reversed == i) && (reversed > highestPalindrom)) {
                highestPalindrom = reversed;
            }
            reversed = 0;
            originalNumber = i;

        }

        return highestPalindrom;
    }

    // alinea e)
    public static long CountPalindromInInterval(int lowestRange, int highestRange) {
        int i = lowestRange;
        long originalNumber = 0;
        long highestPalindrom = 0;
        long counter = 0;
        for (i = lowestRange; i <= highestRange; i++) {
            long reversed = 0;
            originalNumber = i;
            while (originalNumber > 0) {
                reversed = reversed * 10 + (originalNumber % 10);
                originalNumber = originalNumber / 10;
            }
            if (reversed == i) {
                counter++;
            }
            reversed = 0;
            originalNumber = i;

        }

        return counter;
    }

    // alinea f
    public static long FirstArmstrongNumber(int lowestRange, int highestRange) {
        long sumOfCubes = 0;
        long number = 0;
        for (int i = lowestRange; i <= highestRange; i++) {
            number = i;
            while (number > 0) {
                sumOfCubes = (long) (sumOfCubes + Math.pow(number % 10, 3));
                number = number / 10;
            }
            if (sumOfCubes == i) {
                number =i;
                break;
            }
            sumOfCubes = 0;
        } return number;

    }

    // alinea g
    public static long CountArmstrongNumber(int lowestRange, int highestRange) {
        long sumOfCubes = 0;
        long number = 0;
        long count=0;
        for (int i = lowestRange; i <= highestRange; i++) {
            number = i;
            while (number > 0) {
                sumOfCubes = (long) (sumOfCubes + Math.pow(number % 10, 3));
                number = number / 10;
            }
            if (sumOfCubes == i) {
                count++;
            }
            sumOfCubes = 0;
        } return count++;

    }
}


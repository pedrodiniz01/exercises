import java.util.Scanner;

public class ex8 {
    public static void main(String[] args) {
        System.out.println(sumUntilHighestRange());
    }

    public static int sumUntilHighestRange() {
        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        int highestRange = 0;
        int number = 0;
        int sumNumber=0;
        int lowestNumber=111111111;

        // Pedir dados
        while (highestRange <= 0) {
            System.out.println("Digite o valor máximo: ");
            highestRange = ler.nextInt();
        }

        // Introduzir dados
        while (sumNumber < highestRange) {
            System.out.println("Digite um número: ");
            number = ler.nextInt();
            sumNumber = number + sumNumber;
            if(number<lowestNumber) {
                lowestNumber= number;
            }
        }
       return lowestNumber;
    }
}

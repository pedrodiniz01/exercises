package Teste1;

import java.util.Arrays;

public class Matrix {

    // atributo
    private Integer[][] matrix;

    public Matrix(Integer[][] matrix) {
        this.matrix = matrix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix1 = (Matrix) o;
        return Arrays.deepEquals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(matrix);
    }

    @Override
    public String toString() {
        return "Array{" +
                "array=" + Arrays.deepToString(matrix) +
                '}';
    }

    /**
     * exercicio 1d)
     *
     * @param value that you want to remove from the matrix.
     * @return the matrix without the first value that was selected as parameter. If the value appears twice it only removes the first one.
     */

    public Matrix removeValue(int value) {
        int len = 0;
        int p = 0;
        int a = 0;
        int l = 0;
        int o = 0;
        String val = "";

        Integer[] newMatrix = new Integer[0];
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (matrix[i][k] != value) {
                    l++;
                }
            }
            len = matrix[i].length;
            if ((len == l) || (p != 0)) {
                matrix[i] = Arrays.copyOf(matrix[i], matrix[i].length);
                newMatrix = Arrays.copyOf(matrix[i], matrix[i].length);
                val = "sim";
            }
            if ((len != l) && (val != "sim")) {
                newMatrix = Arrays.copyOf(matrix[i], matrix[i].length);
                matrix[i] = Arrays.copyOf(matrix[i], matrix[i].length - 1);
            }
            a = 0;
            for (int u = 0; u < newMatrix.length; u++) {
                if (newMatrix[u] == value) {
                    o++;
                }
                if (o != 1) {
                    matrix[i][a] = newMatrix[u];
                    a++;
                }
                if (o == 1) {
                    o++;
                    p++;
                }
                l = 0;
            }
        }
        return this;
    }

    /**
     * exercicio 1e)
     *
     * @return True if the matrix is empty, false if not.
     */

    public boolean isTheMatrixEmpty() {
        boolean result = false;
        if (matrix.length == 0) {
            result = true;
        }
        return result;
    }

    /**
     * exercicio 1f)
     *
     * @return the highest value from a matrix.
     */

    public int highestValueInTheMatrix() {
        int highestValue = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (matrix[i][k] >= highestValue) {
                    highestValue = matrix[i][k];
                }
            }
        }
        return highestValue;
    }

    /**
     * exercicio 1g)
     *
     * @return the lowest value from a matrix.
     */

    public int lowestValueInTheMatrix() {
        int lowestValue = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if (matrix[i][k] <= lowestValue) {
                    lowestValue = matrix[i][k];
                }
            }
        }
        return lowestValue;
    }

    /**
     * exercicio 1h)
     *
     * @return the average of the elements of an array.
     */

    public int averageOfTheElementsOfAnArray() {
        int c = 0;
        int soma = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                soma = soma + matrix[i][k];
                c++;
            }
        }
        return soma / c;
    }

    /**
     * exercicio 1i)
     *
     * @return one or more columns where each column represents the sum of each line.
     */


    public Integer[] sumOfEachLineMatrix() {
        Integer[] result = new Integer[matrix.length];
        Integer sum = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                sum = sum + matrix[i][k];
            }
            result[i] = sum;
            sum = 0;
        }
        return result;
    }

    public Integer lineWithHighestColumns() {

        Integer lineWithHighestColumns = 0;
        int c = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                c++;
            }
            if (c >= lineWithHighestColumns) {
                lineWithHighestColumns = c;
            }
            c = 0;
        }
        return lineWithHighestColumns;
    }

    /**
     * exercicio 2j)
     *
     * @return an array with a sum of its columns.
     */
    public Integer[] sumOfitsColumn() {
        int sum = 0;
        int l = 0;
        int len = this.lineWithHighestColumns();
        Integer[] array = new Integer[len];

        for (int i = 0; i < matrix[i].length; i++) {
            for (int k = 0; k < matrix.length; k++) {
                sum = sum + matrix[k][i];
            }
            array[l] = sum;
            sum = 0;
            if (l == len - 1) {
                break;
            } else {
                l++;
            }
        }
        return array;
    }

    /**
     * exercicio 1k)
     *
     * @return the line (index) with the highest sum.
     */

    public Integer indexOfTheLineWithHighestSum() {

        Integer[] array = Arrays.copyOf(this.sumOfEachLineMatrix(), this.sumOfEachLineMatrix().length);
        int highestIndex = 0;
        int index = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= highestIndex) {
                highestIndex = array[i];
                index = i;
            }
        }
        return index;
    }

    /**
     * exericio 1l)
     *
     * @return true if the matrix is quadratic.
     */
    public boolean isTheMatrixQuadratic() {
        boolean result = false;
        int nextLenght = 0;


        for (int i = 0; i < matrix.length; i++) {
            if (i == matrix.length - 1) {
                nextLenght = i;
            } else {
                nextLenght = matrix[i + 1].length;
            }
            if (matrix[i].length == nextLenght) {
                result = true;
            }
            if (matrix[i].length == matrix.length) {
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }

    /**
     * 1 m)
     *
     * @return if the matrix is simmetric.
     */

    public boolean isTheMatrixSimmetric() {

        Integer[] firstColumn = new Integer[matrix.length];
        Integer[] firstLine = new Integer[matrix[0].length];
        Integer[] lastLine = new Integer[matrix[matrix.length - 1].length];
        Integer[] lastColumn = new Integer[matrix.length];


        boolean result = false;


        int l = 0;

        // first column
        for (int i = 0; i < matrix.length; i++) {
            firstColumn[l] = matrix[i][0];
            l++;
        }

        l = 0;

        // first line

        for (int i = 0; i < matrix[0].length; i++) {
            firstLine[l] = matrix[0][i];
            l++;
        }

        l = 0;

        // last line

        for (int i = 0; i < matrix[matrix.length - 1].length; i++) {
            lastLine[l] = matrix[matrix.length - 1][i];
            l++;
        }

        l = 0;

        // last column

        for (int i = 0; i < matrix.length; i++) {
            lastColumn[l] = matrix[i][matrix.length];
            l++;
        }

        if ((Arrays.deepToString(firstLine).equals(Arrays.deepToString(lastLine))) && (Arrays.deepToString(firstColumn).equals(Arrays.deepToString(lastColumn)))) {
            result = true;
        }

        return result;

    }

    /**
     * 1 n)
     *
     * @return counts the elements not null from the main diagonal.
     */

    public Integer numberOfElementsInMainDiagonal() {
        int c = 0;
        int resultFinal = 0;
        boolean result = false;
        if (this.isTheMatrixQuadratic()) {
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i][i] != null) {
                    c++;
                }
            }
            resultFinal = c;
        } else {
            resultFinal = -1;
        }
        return resultFinal;
    }

    /**
     * 1o)
     *
     * @return true if the main diagonal is equals to secondary diagonal. False if not.
     */
    public boolean isMainDiagonalEqualToSecondaryDiagonal() {

        Integer[] mainDiagonal = new Integer[matrix.length];
        Integer[] secondaryDiagonal = new Integer[matrix.length];

        boolean result = false;
        int l = 0;
        int g = 0;
        int u = 0;

        if (this.isTheMatrixQuadratic()) {


            for (int i = 0; i < matrix.length; i++) {
                mainDiagonal[l] = matrix[i][i];
                l++;
            }

            for (int k = matrix.length - 1; k >= 0; k--) {
                secondaryDiagonal[g] = matrix[u][k];
                u++;
                g++;
            }

            if (Arrays.deepToString(mainDiagonal).equals(Arrays.deepToString(secondaryDiagonal))) {
                result = true;
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * 1 q)
     *
     * @return the elements with algarism higher then the average of elements.
     */

    public Integer[] elementsWithAlgarismsThenTheAverage() {
        int c = 0;
        int tempNumber = 0;
        int avg = 0;
        int t = 0;

        // contar o número de algarismos de todos os elementos do array

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                tempNumber = matrix[i][k];
                t++;
                while (tempNumber > 0) {
                    tempNumber = tempNumber / 10;
                    c++;
                }
            }
        }
        avg = c / t;
        c = 0;
        t = 0;

        // tamanho novo vetor

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                tempNumber = matrix[i][k];
                c = 0;
                while (tempNumber > 0) {
                    tempNumber /= 10;
                    c++;
                }
                if (c > avg) {
                    t++;
                }
            }
        }
        Integer[] vetor = new Integer[t];

        // passar para o vector

        c = 0;
        t = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                tempNumber = matrix[i][k];
                c = 0;
                while (tempNumber > 0) {
                    tempNumber /= 10;
                    c++;
                }
                if (c > avg) {
                    vetor[t] = matrix[i][k];
                    t++;
                }
            }
        }

        return vetor;
    }

    /**
     * 1 q)
     * Incorreto
     *
     * @return
     */

    public Integer[] elementsWithHigherEvenAlgarismThenAverage() {

        int tempNumber = 0;
        int c = 0;
        int t = 0;
        int lastDigit = 0;
        double avg = 0;

        // ver % pares

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {

                tempNumber = matrix[i][k];
                while (tempNumber > 0) {
                    t++;
                    lastDigit = tempNumber % 10;
                    if (lastDigit % 2 == 0) {
                        c++;
                    }
                    tempNumber /= 10;
                }
            }
        }
        avg = (double) c / (double) t;

        // saber tamanho novo array

        c = 0;
        t = 0;
        int u = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                tempNumber = matrix[i][k];
                c = 0;
                t = 0;
                while (tempNumber > 0) {
                    lastDigit = tempNumber % 10;
                    if (lastDigit % 2 == 0) {
                        c++;
                    }
                    t++;
                    tempNumber /= 10;
                }
                double otherAvg = (double) c / (double) t;
                if (otherAvg > avg) {
                    u++;
                }
            }
        }

        Integer[] finalArray = new Integer[u];
        t = 0;
        int a = 0;

        // passar para o vector

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                tempNumber = matrix[i][k];
                c = 0;
                t = 0;
                while (tempNumber > 0) {
                    lastDigit = tempNumber % 10;
                    if (lastDigit % 2 == 0) {
                        c++;
                    }
                    tempNumber /= 10;
                    t++;
                }
                double otherAvg = (double) c / (double) t;
                if (otherAvg > avg) {
                    finalArray[a] = matrix[i][k];
                    a++;
                }
            }
        }
        return finalArray;
    }

    /**
     * 1 r)
     *
     * @return
     */
    public Integer[][] invertElementsOfItsLine() {
        int a = 0;

        // Copy Array
        Integer[][] newArray = new Integer[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            newArray[i] = matrix[i].clone();
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                int len = matrix[i].length - 1 + a;
                newArray[i][len] = matrix[i][k];
                a--;
            }
            a = 0;
        }
        return newArray;
    }

    /**
     * /todo por fazer
     * 1 q)
     */
    public Integer[][] invertElementsOfAColumn() {

        // Copy Array
        Integer[][] newArray = new Integer[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            newArray[i] = matrix[i].clone();
        }

        // Invert value of each column
        for (int i = 0; i < matrix[0].length; i++) {
            for (int k = 0; k < matrix.length; k++) {
                int len = matrix.length - 1 - k;
                newArray[len][i] = matrix[k][i];

            }
        }
        return newArray;
    }

    public Integer[][] turn90degreesQuadratic() {
        Integer[][] finalArray = new Integer[0][0];
        Integer[][] newArray = new Integer[0][0];

        // Copy Array

        newArray = new Integer[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            newArray[i] = matrix[i].clone();
        }


        int a = 0;

        // transpose

        for (int i = 0; i < newArray.length; i++) {
            for (int k = 0; k < newArray[i].length; k++) {
                newArray[i][k] = matrix[a][i];
                a++;
            }
            a = 0;
        }

        Integer[][] retangularArray = Arrays.copyOf(newArray, newArray.length);

        // Copy newNewArray
        Integer[][] newNewArray = new Integer[newArray.length][];
        for (int i = 0; i < newArray.length; i++) {
            newNewArray[i] = newArray[i].clone();
        }

        a = 0;
        for (int i = 0; i < newArray.length; i++) {
            for (int k = 0; k < newNewArray[0].length; k++, a--) {
                int len = newArray[0].length - 1 + a;
                newNewArray[i][k] = newArray[i][len];
            }
            a = 0;
        }
        /*finalArray = Arrays.copyOf(newNewArray, newNewArray.length);*/

        return newNewArray;
    }

    public Integer[][] turn90degreesRectangular() {
        Integer[][] finalArray = new Integer[0][0];
        Integer[][] newArray = new Integer[0][0];

        // Copy Array

        newArray = new Integer[matrix[0].length][matrix.length];


        int a = 0;

        // transpose

        for (int i = 0; i < newArray.length; i++) {
            for (int k = 0; k < newArray[i].length; k++) {
                newArray[i][k] = matrix[a][i];
                a++;
            }
            a = 0;
        }
        return newArray;


    }

    public Integer[][] turn90degrees() {
        Integer[][] finalArray = new Integer[0][0];
        if (this.isTheMatrixQuadratic()) {

            Integer[][] newArray = new Integer[0][0];

            // Copy Array

            newArray = new Integer[matrix.length][];
            for (int i = 0; i < matrix.length; i++) {
                newArray[i] = matrix[i].clone();
            }


            int a = 0;

            // transpose

            for (int i = 0; i < newArray.length; i++) {
                for (int k = 0; k < newArray[i].length; k++) {
                    newArray[i][k] = matrix[a][i];
                    a++;
                }
                a = 0;
            }

            Integer[][] retangularArray = Arrays.copyOf(newArray, newArray.length);

            // Copy newNewArray
            Integer[][] newNewArray = new Integer[newArray.length][];
            for (int i = 0; i < newArray.length; i++) {
                newNewArray[i] = newArray[i].clone();
            }

            a = 0;
            for (int i = 0; i < newArray.length; i++) {
                for (int k = 0; k < newNewArray[0].length; k++, a--) {
                    int len = newArray[0].length - 1 + a;
                    newNewArray[i][k] = newArray[i][len];
                }
                a = 0;
            }
            /*finalArray = Arrays.copyOf(newNewArray, newNewArray.length);*/

            finalArray = newNewArray;
        } else {

            Integer[][] newArray = new Integer[0][0];

            // Copy Array

            newArray = new Integer[matrix[0].length][matrix.length];


            int a = 0;

            // transpose

            for (int i = 0; i < newArray.length; i++) {
                for (int k = 0; k < newArray[i].length; k++) {
                    newArray[i][k] = matrix[a][i];
                    a++;
                }
                a = 0;
            }
            finalArray = newArray;
        }
        return finalArray;
    }

    public Integer[][] turn180degrees() {
        Integer[][] newArray = new Integer[0][0];

        // Copy Array
        if (this.isTheMatrixQuadratic()) {
            newArray = new Integer[matrix.length][];
            for (int i = 0; i < matrix.length; i++) {
                newArray[i] = matrix[i].clone();
            }
        }
        matrix = newArray;

        matrix = this.turn90degrees();
        matrix = this.turn90degrees();
        newArray = matrix;
        return newArray;
    }

    public Integer[][] turnMinus90Degrees() {
        Integer[][] newArray = new Integer[0][0];
        matrix = this.turn90degrees();
        matrix = this.turn90degrees();
        matrix = this.turn90degrees();
        newArray= this.matrix;
        return newArray;
    }

}

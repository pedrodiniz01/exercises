public class ex14 {
    public static boolean distanciaPositiva(double a) {
        if (a>=0) {
            return true;
        } return false;
    }
    public static double conversaoMilhasParaKilometros(double milhas) {
        if (distanciaPositiva(milhas) == true) {
            return milhas*1609/1000;
        }
        return 0;
    }
    public static double distanciaMediaDiariaKilometros(int dia1, int dia2, int dia3, int dia4, int dia5) {
        return (conversaoMilhasParaKilometros(dia1)+ conversaoMilhasParaKilometros(dia2)+ conversaoMilhasParaKilometros(dia3) + conversaoMilhasParaKilometros(dia4)+conversaoMilhasParaKilometros(dia5))/5;
    }
}

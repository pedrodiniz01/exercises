import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        System.out.println(algo(0));
        System.out.println(algo(-5));
        System.out.println(algo(2));
    }
    public static double algo(double a) {
        if (a < 0) {
            return a;
        }
        else if ( a == 0) {
            return 0;
        }
        else if (a>0) {
            return Math.pow(a,2) - 2*a;
        }
        return a;
    }
}

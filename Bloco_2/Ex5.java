import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {

        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        int area;

        // pedir dados
        System.out.println("Introduza a area: ");
        area = ler.nextInt();

        // saída dados
        volumeString(area);


    }

    public static String volumeString(double a) {
        if (a > 0) {
            double aresta = Math.sqrt(a / 6);
            double volume = Math.pow(aresta, 3);
            System.out.printf("O volume do cubo é: %.5f", volume);
        } else {
            System.out.println("Valor da área incorreto.");
        }
        return null;
    }

    public static double volume(double a) {
        if (a > 0) {
            double aresta = Math.sqrt(a / 6);
            double volume = Math.pow(aresta, 3);
            return volume;
        }

        return 0;
    }

    public static String classificacao(double a) {
        if (a <= 1) {
            return "Pequeno";
        } else if (a > 1 && a <= 2) {
            return "Médio";
        } else if (a > 2) {
            return "Grande";
        }

        return null;
    }
}


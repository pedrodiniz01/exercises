public class ex18_refazer {
    public static void main(String[] args) {
        int segundosDuracao = 2330;

        System.out.println((int) segundosDuracao / 3600);
        System.out.println((int) (((double) segundosDuracao / 3600 - ((int) segundosDuracao / 3600)) * 60));
        System.out.println((int) (((((double) segundosDuracao / 3600 - ((int) segundosDuracao / 3600)) * 60) - ((int) (((double) segundosDuracao / 3600 - ((int) segundosDuracao / 3600)) * 60))) * 60));
        System.out.println();

        System.out.println(fimProcessamento(23, 59, 0, segundosDuracao));

    }

    public static String fimProcessamento(int horasInicio, int minutosInicio, int segundosInicio, int segundosDuracao) {
        int conversaoSegundosDuracaoParaHoras = (int) segundosDuracao / 3600;
        int conversaoSegundosDuracaoParaMinutos = (int) (((double) segundosDuracao / 3600 - ((int) segundosDuracao / 3600)) * 60);
        int conversaoSegundosDuracaoParaSegundos = (int) (((((double) segundosDuracao / 3600 - ((int) segundosDuracao / 3600)) * 60) - ((int) (((double) segundosDuracao / 3600 - ((int) segundosDuracao / 3600)) * 60))) * 60);

        int horasFim = 0;
        int minutosFim = 0;
        int segundosFim = 0;

        if (horasInicio >= 0 && horasInicio <= 24) {
            if (horasInicio + conversaoSegundosDuracaoParaHoras >= 24) {
                horasFim = (horasInicio + conversaoSegundosDuracaoParaHoras) - 24;
            } else if (((horasInicio + conversaoSegundosDuracaoParaHoras) == 23) && (minutosInicio + conversaoSegundosDuracaoParaMinutos) >= 60) {
                horasFim = 00;
            } else if (((horasInicio + conversaoSegundosDuracaoParaHoras) == 23) && ((minutosInicio + conversaoSegundosDuracaoParaMinutos) == 59) && ((segundosInicio + conversaoSegundosDuracaoParaSegundos) >= 60)) {
                horasFim = 00;
            } else if (minutosInicio + conversaoSegundosDuracaoParaMinutos > 60) {
                horasFim = horasInicio + conversaoSegundosDuracaoParaHoras + 1;
            } else if (((minutosInicio + conversaoSegundosDuracaoParaMinutos) == 59) && ((segundosInicio + conversaoSegundosDuracaoParaSegundos) >= 60)) {
                horasFim = horasInicio + conversaoSegundosDuracaoParaHoras + 1;
            } else if (horasInicio + conversaoSegundosDuracaoParaHoras < 24){
                horasFim = (horasInicio + conversaoSegundosDuracaoParaHoras);
            }
        }
        if (minutosInicio >= 0 && minutosInicio <= 60) {
            if ((minutosInicio + conversaoSegundosDuracaoParaMinutos >= 60) && (horasFim == 00)) {
                minutosFim = (minutosInicio + conversaoSegundosDuracaoParaMinutos) - 60;
                horasFim = horasFim + 1;
            } else if ((minutosInicio + conversaoSegundosDuracaoParaMinutos >= 60) && (horasFim != 00) && (minutosInicio + conversaoSegundosDuracaoParaHoras >24)) {
                minutosFim = (minutosInicio + conversaoSegundosDuracaoParaMinutos) - 60;
                horasFim = horasFim + 1;
            }
            else{
                minutosFim = (minutosInicio + conversaoSegundosDuracaoParaMinutos);
            }

        }
        if (segundosInicio >= 0 && segundosInicio <= 60) {
            if (segundosInicio + conversaoSegundosDuracaoParaSegundos > 60) {
                segundosFim = (segundosInicio + conversaoSegundosDuracaoParaSegundos) - 60;
            } else {
                segundosFim = (segundosInicio + conversaoSegundosDuracaoParaSegundos);
            }
        }
        return "Horas de terminado prevista:\n" + horasFim + " horas e " + minutosFim + " minutos e " + segundosFim + " segundos.";
    }
}

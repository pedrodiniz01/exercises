public class ex15 {
    public static boolean validacaoLadosPositivos(double a, double b, double c) {
        if ((a > 0 && b > 0 && c > 0) && (a<b+c) && (b<a+c) && (c<a+b)){
            return true;
        }
        return false;
    }

    public static String avaliarTriangulo(double a, double b, double c) {
        if (validacaoLadosPositivos(a, b, c) == true) {
            if ((a == b) && (a != c)) {
                return "Triângulo Isosceles!";
            } else if ((a == b) && (a == c)) {
                return "Triângulo Equilátero!";
            } else if ((a != b) && (a != c) && (c != b)) {
                return "Triângulo Escaleno!";

            }

        } return "Um dos seguintes erros:\n1) Um ou mais lados não são superiores a 0.\n2) O tamanho de um dos lados é superior à soma dos outros dois.";
    }

}

public class ex9 {
    public static void main(String[] args) {

    } public static boolean tresDigitos(int a) {
        if (a>=100 && a<=999) {
            return true;
        } return false;
    }
    public static String crescenteDecrescente (int a) {
        int dig1 = (a/100)%10;
        int dig2 = (a/10)%10;
        int dig3 = a%10;
        if ( tresDigitos(a) == true) {
            if((dig1<dig2) && (dig2<dig3)) {
                return "Algarismos organizados por ordem crescente.";
            }
            else if ((dig1>dig2) && (dig2>dig3)) {
                return "Algarismos organizados por ordem decrescente.";
            }
            else return "Algarismos não estão organizados por ordem crescente nem decrescente.";
        } return "Número introduzido não tem 3 algarismos.";
    }
}

//todo falta fazer testes

public class Ex6 {
    public static void main(String[] args) {
        System.out.println("Teste: " + (double) 86400 / 3600);
        System.out.println("");
        System.out.println(86400 + " segundos equivale a " + conversionSecondsToHours(86400) + conversionSecondsToMinutes(86400) + conversionFinalToSeconds(86400));
        System.out.println("");
        System.out.println(partOfDay(25065));
    }

    public static boolean lessThanDayInSeconds(int seconds) {
        if (seconds >= 0 && seconds <= 86400) {
            return true;
        }
        return false;
    }

    public static String conversionSecondsToHours(int seconds) {
        if (lessThanDayInSeconds(seconds) == true) {
            int hoursWithoutDecimal = (int) seconds / 3600;
            return hoursWithoutDecimal + " horas : ";
        }

        return null;
    }

    public static String conversionSecondsToMinutes(int seconds) {
        if (lessThanDayInSeconds(seconds) == true) {
            double decimalPartOfInteger = ((double) seconds / 3600) - (int) seconds / 3600;
            int conversionDecimalToMinutes = (int) (decimalPartOfInteger * 60);
            return conversionDecimalToMinutes + " minutos : ";
        }
        return null;
    }

    public static String conversionFinalToSeconds(int seconds) {
        double decimalPartOfInteger = ((((double) seconds / 3600) - (int) seconds / 3600) * 60);
        double conversionDecimalToMinutes = (int) (decimalPartOfInteger);
        int finalSeconds = (int) (((double) decimalPartOfInteger - (int) conversionDecimalToMinutes) * 60);
        return finalSeconds + " segundos.";
    }

    public static String partOfDay(int seconds) {
        if (lessThanDayInSeconds(seconds) == true) {
            if (seconds >= 21600 && seconds < 43201) {
                return "Bom dia!";
            } else if (seconds >= 43201 && seconds < 72001) {
                return "Boa tarde!";
            } else if (seconds >= 72001 && seconds < 21600) {
                return "Boa noite!";
            }
        }
        return null;
    }
}



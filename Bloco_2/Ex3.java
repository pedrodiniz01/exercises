import java.util.Scanner;
public class Ex3 {
    public static void main(String[] args) {
        // variaveis
        double a,b,c,d ;

        // scanner
        Scanner ler = new Scanner(System.in);

        // pedir dados
        System.out.println("Introduzir os vários dados: ");

        a = ler.nextDouble();
        b = ler.nextDouble();
        c = ler.nextDouble();
        d = ler.nextDouble();

        System.out.println("A distância é de: ");
        System.out.println(distanciaEntrePontos(a,b,c,d));
    }
    public static double distanciaEntrePontos(double a,double b, double c, double d) {
        double formula = Math.sqrt(Math.pow((a-b),2)+Math.pow((c-d),2));
        return formula;
    }
    }


import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {

        // scanner
        Scanner ler = new Scanner(System.in);

        // variaveis
        int nota1, nota2, nota3;
        double peso1, peso2, peso3;

        // pedir dados
        System.out.println("\nDigite as 3 notas: ");
        nota1 = ler.nextInt();
        nota2 = ler.nextInt();
        nota3 = ler.nextInt();

        System.out.println("Digite os 3 pesos: ");
        peso1 = ler.nextDouble();
        peso2 = ler.nextDouble();
        peso3 = ler.nextDouble();

        // saide de dados
        System.out.println("A sua média é de: ");
        System.out.println(mediaPesada(nota1, nota2, nota3, peso1, peso2, peso3));
        System.out.println("\nEstás: ");
        System.out.println(avaliacao(mediaPesada(nota1, nota2, nota3, peso1, peso2, peso3)));

    }

    public static boolean validacaoPesos(double a, double b, double c) {
        if ((a > 0) && (b > 0) && (c > 0)) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean validacaoNotas(int a, int b, int c) {
        if ((a >= 0 && a <= 20) && (b >= 0 && b <= 20) && (c >=0 && c <= 20)) {
            return true;
        } else {
            return false;
        }

    }

    public static double mediaPesada(int a, int b, int c, double d, double e, double f) {

        // dados
        double mediaPesada;

        // estrutura decisão

        if ((Ex1.validacaoNotas(a, b, c) == true) && (Ex1.validacaoPesos(a, b, c) == true)) {
            mediaPesada = (a * d + b * e + c * f) / (d + e + f);
            return mediaPesada;
        } else {
            return Double.NaN;
        }
    }

    public static String avaliacao(double a) {
        if (a > 0 && a <= 20) {
            if (a >= 8 ) {
                return "Aprovado!";
            }
            return "Reprovado!";
        } else {
            return "NaN";
        }
    }
}


public class ex17 {
    public static void main(String[] args) {
        System.out.println(horaMinutosChegada(22, 24, 2, 23));
    }

    public static boolean validacaoHorasMinutos(int horas, int minutos) {
        if (((horas >= 0 && horas <= 24) && (minutos >= 0 && minutos <= 60)) == true) {
            return true;
        }
        return false;
    }

    public static String horaMinutosChegada(int horasPartida, int minutosPartida, int horasViagem, int minutosViagem) {

        if ((validacaoHorasMinutos(horasPartida, minutosPartida)) && (validacaoHorasMinutos(horasViagem, minutosViagem))) {
            int horasChegada;
            int minutosChegada;
            if ((horasPartida + horasViagem >= 24) && (minutosPartida + minutosViagem >= 60)) {
                horasChegada = (horasPartida + horasViagem) - 24 + 1;
                minutosChegada = (minutosPartida + minutosViagem) - 60;
            } else if ((horasPartida + horasViagem >= 24) && (minutosPartida + minutosViagem < 60)) {
                horasChegada = (horasPartida + horasViagem) - 24;
                minutosChegada = minutosPartida + minutosViagem;
            } else if ((horasPartida + horasViagem == 23) && (minutosPartida + minutosViagem >= 60)) {
                horasChegada = 0;
                minutosChegada = (minutosPartida + minutosViagem) - 60;
            } else {
                horasChegada = horasPartida + horasViagem;
                minutosChegada = minutosPartida + minutosViagem;

            }

            return "O comboio chegará às " + horasChegada + ":" + minutosChegada + ".";

        }

        return "Digitou horas/minutos errados.";
    }
}
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        // variaveis
        int numero;

        // scanner
        Scanner ler = new Scanner(System.in);

        // pedir dados
        System.out.println("Digite um número: ");
        numero = ler.nextInt();

        // saida
        System.out.println(SepararDigitos(numero));
        System.out.println(ParImpar(numero));
    }

    public static boolean TresDigitos(int a) {
        if (a < 100 || a > 999) {
            return true;
        }
        return false;
    }

    public static String SepararDigitos(int a) {
        if (TresDigitos(a) == true) {
            return "Não tem 3 digitos.";
        }
         int digito3 = a % 10;
        int digito2 = (a / 10) % 10;
        int digito1 = (a / 100) % 10;
        return (digito1 + ", " + digito2 + ", " + digito3);
    }

    public static String ParImpar(int a) {
        if (a % 2 == 0) {
            return "É par!";
        }
        return "É impar!";
    }

}

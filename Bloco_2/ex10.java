public class ex10 {
    public static boolean precoMaiorIgualQueZero(double a) {
        if (a >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public static double desconto(double a) {
        if (precoMaiorIgualQueZero(a) == true) {
            if (a > 200) {
                return 0.6*a;
            } else if (a > 100 && a <= 200) {
                return 0.4 * a ;
            } else if (a > 50 && a <= 100) {
                return 0.3;
            } else if (a <= 50) {
                return 0.2;
            }

        }
        return 0;

    }
}


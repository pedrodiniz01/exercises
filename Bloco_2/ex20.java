public class ex20 {
    public static void main(String[] args) {

        System.out.println(valorAluguer("segunda","a"));
        System.out.println(valorAluguer("terca","b"));
        System.out.println(valorAluguer("sabado","b"));
        System.out.println(valorAluguer("domingo","c"));
    }

    public static boolean diaDaSemana(String dia) {
        String[] diaDaSemana = {"segunda", "terca", "quarta", "quinta", "sexta", "sabado", "domingo"};
        for (int i = 0; i < diaDaSemana.length; i++) {
            if (diaDaSemana[i] == dia) {
                return true;
            }
        }
        return false;
    }

    public static boolean tipoKit(String kit) {
        String[] tipoKit = {"a", "b", "c"};
        for (int i = 0; i < tipoKit.length; i++) {
            if (tipoKit[i] == kit) {
                return true;
            }
        }
        return false;
    }

    public static int valorAluguer(String dia, String kit) {
        String[] diaDaSemana = {"segunda", "terca", "quarta", "quinta", "sexta"};
        String[] fimDaSemana = {"sabado", "domingo"};
        String[] tipoAluguer = {"a", "b", "c"};
        if (((tipoKit(kit) == true)) && ((diaDaSemana(dia) == true))) {
            for (int i = 0; i < diaDaSemana.length; i++) {
                if ((diaDaSemana[i] == dia) && (kit == "a")) {
                    return 30;
                } else if ((diaDaSemana[i] == dia) && (kit == "b")) {
                    return 50;
                }
                if ((diaDaSemana[i] == dia) && (kit == "c")) {
                    return 100;
                }
            }
            for (int i = 0; i < fimDaSemana.length; i++) {
                if((fimDaSemana[i]==dia) && (kit == "a")) {
                    return 40;
                }
                else if((fimDaSemana[i]==dia) && (kit == "b")) {
                    return 70;
                }
                else if((fimDaSemana[i]==dia) && (kit == "c")) {
                    return 140;
                }
            }
        }


        return 0;
    }
}

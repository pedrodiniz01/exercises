public class ex8 {
    public static void main(String[] args) {
        System.out.println(avaliarMultiplos(2,13));
    }

    public static String avaliarMultiplos(int num1, int num2) {
        if ((num1 % num2 == 0) && (num2 % num1 != 0)) {
        return num1 + " é multiplo de " + num2+ ".";
        }
        else if ((num2 % num1 == 0) && (num1 % num2 != 0)) {
            return num2 + " é multiplo de " + num1 + ".";
        }
        else if ((num2 % num1 != 0) && (num1 % num2 != 0)) {
            return num1 + " não é múltiplo nem divisor de " + num2 + ".";
        } return null;
    }
}

